import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

public class Zal_1_Poprawna_Rejestracja extends SeleniumBaseTest {
    @DataProvider
    public static Object[][] randomEmail() {
        String allowedChars = "abcdefghijklmnopqrstuvwxyz" + "1234567890" + "_-.";
        int length = 10;
        String temp = RandomStringUtils.random(length, allowedChars);
        String email = temp.substring(0, temp.length() - 9) + "@test.com";
        return new Object[][] {
                { email, "Test1!", "Test1!"}
        };
    }
    @Test(dataProvider = "randomEmail")
    public void registrationDifferentPasswords(String email, String password, String passwordConf) {
        new LoginPage(driver)
                .registrationGo()
                .typeEmailregist(email)
                .typePassword(password)
                .typePasswordConf(passwordConf)
                .submitLoginRegi();
        new HomePage(driver)
                .assertWelcomeElementIsShown();
    }
}

