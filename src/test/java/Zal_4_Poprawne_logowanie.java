import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Zal_4_Poprawne_logowanie extends SeleniumBaseTest {

    @DataProvider
    public static Object[][] correctPasswordandEmail() {
        return new Object[][] {
                { "test@test.com", "Test1!"}
        };
    }

    @Test (dataProvider = "correctPasswordandEmail")
    public void correctLoginTest(String email, String password) {
        new LoginPage(driver)
                .typeEmail(email)
                .typePassword(password)
                .submitLogin()
                .assertWelcomeElementIsShown();
    }

}
