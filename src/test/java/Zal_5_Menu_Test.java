import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Zal_5_Menu_Test extends SeleniumBaseTest {

    @DataProvider
    public static Object[][] correctPasswordandEmail() {
        return new Object[][] {
                { "test@test.com", "Test1!"}
        };
    }

    @Test(dataProvider = "correctPasswordandEmail")
    public void menuProcessesTest(String email, String password) {
        new LoginPage(driver)
                .typeEmail(email)
                .typePassword(password)
                .submitLogin()
                .goToProcesses()
                .assertPageUrlCorrect()
                .assertPageTitleCorrect();
    }

    @Test(dataProvider = "correctPasswordandEmail")
    public void menuCharacteristicsTest(String email, String password) {
        new LoginPage(driver)
                .typeEmail(email)
                .typePassword(password)
                .submitLogin()
                .goToCharacteristics()
                .assertPageUrlCorrect()
                .assertPageTitleCorrect();
    }
}
