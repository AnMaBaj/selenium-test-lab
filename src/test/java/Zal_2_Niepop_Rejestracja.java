import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.RegistrationPage;

public class Zal_2_Niepop_Rejestracja extends SeleniumBaseTest {

    @DataProvider
    public static Object[][] differentPasswords() {
        return new Object[][] {
                { "test@test.com", "Test1!", "Test11"}
        };
    }
    @Test(dataProvider = "differentPasswords")
    public void registrationDifferentPasswords(String email, String password, String passwordConf) {
        new LoginPage(driver)
                .registrationGo()
                .typeEmailregist(email)
                .typePassword(password)
                .typePasswordConf(passwordConf)
                .submitLoginRegi()
                .assertPasswordErrorIsShown("The password and confirmation password do not match.");
    }
}
