import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Zal_6_Poprawne_Dodanie_Procesu extends SeleniumBaseTest {
    @DataProvider
    public static Object[][] correctPasswordandEmail() {
        return new Object[][] {
                { "test@test.com", "Test1!"}
        };
    }

    @Test(dataProvider = "correctPasswordandEmail")
    public void correctLoginTest(String email, String password) {
        new LoginPage(driver)
                .typeEmail(email)
                .typePassword(password)
                .submitLogin()
                .goToProcesses()
                .addNewProcess()
                .typeName("Proces1")
                .typeDescriptione("Description")
                .typeNotes("Notes")
                .createProces()
                .assertNewProcesAddCorect("Proces1");
    }


}
