import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Zal_3_Niepop_Rejetracja_Haslo_niespelniajace_Wymagan extends SeleniumBaseTest {

    @DataProvider
    public static Object[][] differentWrongPasswords() {
        return new Object[][] {
                {"test1!", "Passwords must have at least one uppercase ('A'-'Z')."},
                {"Test!!", "Passwords must have at least one digit ('0'-'9')."},
                {"Test11", "Passwords must have at least one non alphanumeric character."}

        };
    }

    @Test(dataProvider = "differentWrongPasswords")
    public void registrationWrongPasswords(String password, String errorText ) {
        new LoginPage(driver)
                .registrationGo()
                .typeEmailregist("nna@test.com")
                .typePassword(password)
                .typePasswordConf(password)
                .submitLoginRegi()
                .assertWrongPasswordIsShown(errorText);
    }
}
