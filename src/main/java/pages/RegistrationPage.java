package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class RegistrationPage {
    protected WebDriver driver;

    public RegistrationPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailTxtReg;

    @FindBy(css = "button[type=submit]")
    private WebElement registerBtn;

    @FindBy(id = "Email-error")
    private WebElement emailError;

    @FindBy(id = "ConfirmPassword-error")
    private WebElement passwordError;

    @FindBy(css = ".validation-summary-errors ul li")
    private WebElement wrongPassword;

    @FindBy(id = "Password")
    private WebElement passwordTxtReg;

    @FindBy(id = "ConfirmPassword")
    private WebElement passwordConfirmTxtReg;


    public RegistrationPage typeEmailregist(String email) {
        emailTxtReg.clear();
        emailTxtReg.sendKeys(email);
        return this;
    }
    public RegistrationPage typePassword(String password) {
        passwordTxtReg.clear();
        passwordTxtReg.sendKeys(password);
        return this;
    }

    public RegistrationPage typePasswordConf(String passwordConf) {
        passwordConfirmTxtReg.clear();
        passwordConfirmTxtReg.sendKeys(passwordConf);
        return this;
    }

    public RegistrationPage submitLoginRegi() {
        registerBtn.click();
        return this;
    }
    public RegistrationPage assertEmailErrorIsShown(String expError) {
        Assert.assertTrue((emailError.isDisplayed()));
        Assert.assertEquals(emailError.getText(), expError);

        return this;

    }
    public RegistrationPage assertPasswordErrorIsShown(String expError) {
        Assert.assertTrue((passwordError.isDisplayed()));
        Assert.assertEquals(passwordError.getText(), expError);

        return this;
    }

    public RegistrationPage assertWrongPasswordIsShown(String expError) {
        Assert.assertTrue((wrongPassword.isDisplayed()));
        Assert.assertEquals(wrongPassword.getText(), expError);

        return this;
    }
}
