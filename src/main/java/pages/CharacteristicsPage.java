package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CharacteristicsPage {
    protected WebDriver driver;

    public CharacteristicsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".title_left>h3")
    private WebElement titleElm;



    public CharacteristicsPage assertPageUrlCorrect() {
        Assert.assertTrue(driver.getCurrentUrl().endsWith("Characteristics"));
        return this;
    }

    public CharacteristicsPage assertPageTitleCorrect() {
        Assert.assertTrue(titleElm.getText().contentEquals("Characteristics"));
        return this;
    }

}
