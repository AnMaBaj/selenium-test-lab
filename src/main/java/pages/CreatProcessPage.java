package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreatProcessPage {
    protected WebDriver driver;
    public CreatProcessPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    @FindBy(id = "Name")
    private WebElement nameField;

    @FindBy(id = "Description")
    private WebElement descriptionField;

    @FindBy(id = "Notes")
    private WebElement notesField;

    @FindBy(css = "input[type=submit]")
    private WebElement createBtn;


    public CreatProcessPage typeName(String text) {
        nameField.clear();
        nameField.sendKeys(text);
        return this;
    }
    public CreatProcessPage typeDescriptione(String text) {
        descriptionField.clear();
        descriptionField.sendKeys(text);
        return this;
    }
    public CreatProcessPage typeNotes(String text) {
        notesField.clear();
        notesField.sendKeys(text);
        return this;
    }

    public ProcessesPage createProces() {
        createBtn.click();
        return new ProcessesPage(driver);
    }
}
