package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;


public class ProcessesPage {
    protected WebDriver driver;


    public ProcessesPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "a[href*='Projects/Create'")
    private WebElement addProcessButton;

    @FindBy(css = ".title_left>h3")
    private WebElement titleElm;

    @FindBy(css = ".jambo_table tr:last-of-type td:first-of-type")
    private WebElement newProcesName;

    public CreatProcessPage addNewProcess() {
        addProcessButton.click();
        return new CreatProcessPage(driver);
    }
    public ProcessesPage assertPageUrlCorrect() {
        Assert.assertTrue(driver.getCurrentUrl().endsWith("Projects"));
        return this;
    }

    public ProcessesPage assertPageTitleCorrect() {
        Assert.assertTrue(titleElm.getText().contentEquals("Processess"));
        return this;
    }
    public ProcessesPage assertNewProcesAddCorect(String name) {
        Assert.assertTrue(newProcesName.getText().contentEquals(name));
        return this;
    }



}
