package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class LoginPage {
    protected WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email-error")
    private WebElement emailError;

    @FindBy(css = "a[href*=Register")
    private WebElement registerButton;

    @FindBy(css = ".flash-message>strong")
    private WebElement logoutMsg;

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(id = "Password")
    private WebElement passwordTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement loginBtn;

    @FindBy(css = ".validation-summary-errors>ul>li")
    public List<WebElement> loginErrors;

    public LoginPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public LoginPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);

        return this;
    }

    public HomePage submitLogin() {
        loginBtn.click();
        return new HomePage(driver);
    }
    public RegistrationPage registrationGo() {
        registerButton.click();
        return new RegistrationPage(driver);
    }


    public LoginPage submitLoginWithFailure() {
        loginBtn.click();
        return this;
    }

    public LoginPage assertLoginErrorIsShown(String expError) {
        boolean doesErrorExists = loginErrors
                .stream()
                .anyMatch(error -> error.getText().equals(expError));
        Assert.assertTrue(doesErrorExists);
        return this;

    }

    public LoginPage assertEmailErrorIsShown(String expError) {
        Assert.assertTrue((emailError.isDisplayed()));
        Assert.assertEquals(emailError.getText(), expError);

        return this;

    }

    public LoginPage assertUserSuccessfullyLogout() {
        Assert.assertTrue(logoutMsg.isDisplayed());
        Assert.assertEquals(logoutMsg.getText(), "User succesfully logged out");
        return this;

    }

}
